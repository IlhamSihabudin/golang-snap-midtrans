package Controllers

import (
	"GoMidtrans/Config"
	"github.com/gin-gonic/gin"
	"github.com/midtrans/midtrans-go"
	"github.com/midtrans/midtrans-go/example"
	"github.com/midtrans/midtrans-go/snap"
	"net/http"
)

func InitConnectionMidtrans() (client snap.Client) {
	midtrans.ServerKey = Config.SERVER_KEY
	midtrans.Environment = midtrans.Sandbox

	client.New(Config.SERVER_KEY, midtrans.Sandbox)

	return client
}

func GetSnapTransaction(c *gin.Context) {
	s := InitConnectionMidtrans()

	custAddress := &midtrans.CustomerAddress{
		FName:       "John",
		LName:       "Doe",
		Phone:       "081234567890",
		Address:     "Baker Street 97th",
		City:        "Jakarta",
		Postcode:    "16000",
		CountryCode: "IDN",
	}

	req := &snap.Request{
		TransactionDetails: midtrans.TransactionDetails{
			OrderID:  "MID-GO-ID-" + example.Random(),
			GrossAmt: 200000,
		},
		CreditCard: &snap.CreditCardDetails{
			Secure: true,
		},
		CustomerDetail: &midtrans.CustomerDetails{
			FName:    "John",
			LName:    "Doe",
			Email:    "john@doe.com",
			Phone:    "081234567890",
			BillAddr: custAddress,
			ShipAddr: custAddress,
		},
		EnabledPayments: snap.AllSnapPaymentType,
		Items: &[]midtrans.ItemDetails{
			{
				ID:    "ITEM1",
				Price: 200000,
				Qty:   1,
				Name:  "Someitem",
			},
		},
	}

	snapResp, err := s.CreateTransaction(req)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"message" : err.GetMessage(),
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"data" : snapResp,
	})
}
