package main

import(
	"GoMidtrans/Controllers"
	"github.com/gin-gonic/gin"
)

func main()  {
	router := gin.Default()

	router.POST("/transaction", Controllers.GetSnapTransaction)

	router.Run(":8000")
}
